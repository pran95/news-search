# Brief
• The application should not call the Guardian API directly, rather we'd
like to see you build your own API as a proxy for the Guardian one

• Each item should show:

• The Title
• A link to the article
• The publication date (formatted as DD/MM/YYYY).
• A button or checkbox that allows it to be pinned.
• Pinned items should appear below the search results, and stay
on screen as search results change.


### Install Dependencies 
```
cd News-Search-React
npm install
```
## TO RUN the FRONT_END
```
npm start
```

### INSTALL Dependencies
```
cd api
npm install
```
## TO RUN NODE SERVER
```
node server.js
```
