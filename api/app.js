const express = require('express');
const app = express();
const cors = require('cors');

app.use(cors());

const guardian = require('./guardian/guardian');


// GET Guardian articles
app.use('/guardian', guardian.router);



module.exports = app;
