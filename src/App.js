import React from 'react';
import Main from './Main';
import banner from './banner.png';
import './App.css';



const App = () => {
  return (
    <div className="App">
    <img className="london" src={banner} />

    <div className="strip">
      <p className="header">News Search</p>

    </div>
      <Main />
      <div className="strip2">
      <div className="london2" />
      </div>
    </div>
  )
}

export default App;
