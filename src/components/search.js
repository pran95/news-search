import React, {useEffect, useState} from 'react';
import TextField from '@material-ui/core/TextField';
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import axios from "axios";
import {FormControl, Input, InputLabel, MenuItem, Select} from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import ListItemText from "@material-ui/core/ListItemText";
import ResultCard from "./card";
import { makeStyles } from '@material-ui/core/styles';
import SortIcon from '@material-ui/icons/Sort';
import { red } from '@material-ui/core/colors';




const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 545,


    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
        fontSize: '0.5em'
    },
    formControl: {
        margin: theme.spacing(1),
        maxWidth: 200,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const Search = ({ searchArticles }) => {
    const [text, setText] = useState('');
    const [searchResults, setSearchResults] = useState([]);
    const [favourites, setFavourites] = useState([]);
    const [data, setData] = useState([]);
    const [filterView, setFilterView] = useState(false);
    const [bookmarkView, setBookmarkView] = useState(false);

    const reasons = [
        "world",
        "film",
        "sports",
        "business",
        "technology",
        "politics",
        "education",
        "uk-news",
    ];
    useEffect(() => {
        const Favourites = JSON.parse(
            localStorage.getItem('news-search-app-bookmarks')
        );

        if (Favourites) {
            setFavourites(Favourites);
        }
    }, []);

    const saveToLocalStorage = (items) => {
        localStorage.setItem('news-search-app-bookmarks', JSON.stringify(items));
    };

    const addFavouriteMovie = (story) => {
        const newFavouriteList = [...favourites, story];
        setFavourites(newFavouriteList);
        // console.log(favourites);
        saveToLocalStorage(newFavouriteList);
    };


    const classes = useStyles();

    const handleChange = (e) => {
        setText(e.target.value);
    };
    const handleSubmit = (e) => {
        e.preventDefault();
        axios.get(`http://localhost:5000/guardian/article_search?q=${text}`)
            .then(result => {
                // console.log(result)
                const newStories = result.data.articles

                // console.log(newStories[0])

                setSearchResults(newStories);
            });
        setFilterView(false);
    };
    const SortSection= () =>{

        const sortArr= searchResults.map( (story) => story);
        // console.log(sortArr);
        // console.log(data);
        const sortResults = sortArr.filter(( s) => s.sectionId === data[0]);
        // console.log(sortResults);
        setFilterView(true);
        return(
           <ResultCard searchResults={sortResults} />
        )
    }
    // const Bookmark= (newStories) =>{
    //
    //     // newStories.forEach(function (bookmark) {
    //     //     bookmark.Active = "false";
    //     //     setSearchResults(newStories);
    //     // } );
    //     const newArr = newStories.map(i => { i.bookmark = false; return i; })
    //     setSearchResults(newArr)
    //     // console.log(newArr);
    //
    // }
    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 5.5 + ITEM_PADDING_TOP,
                width: 200,
            },
        },
    };
    return (
        <>
            <div>
                <TextField
                    size="small"
                    style={{
                        paddingTop:'14px',
                        width: "500px",
                        paddingBottom:'20px',}}
                    // label="Search articles"
                    placeholder="Search articles"
                    type="text"
                    name="text"
                    value={text}
                    onChange={handleChange}


                    InputProps={{ style: { fontSize: "2.0rem" } }}
                    onChange={handleChange}
                />
                <IconButton  style={{
                    paddingTop:'37px',
                 }} onClick={handleSubmit}>
                    <SearchIcon />
                </IconButton>

                <FormControl className={classes.formControl}>
                    <InputLabel style={{ fontSize: '0.4em'}} id="demo-multiple-checkbox-label">
                      Sort By
                    </InputLabel>

                    <Select
                        value={data}
                        multiple
                        style={{width: 120, maxHeight: 22, fontSize: '10pt', marginRight: 10, paddingTop:38}}
                        onChange={(e) => setData(e.target.value)}
                        className={classes.selectEmpty}
                        id="demo-multiple-checkbox-label"
                        input={<Input/>}
                        renderValue={(selected) => selected.join(", ")}
                        MenuProps={MenuProps}
                        // label="sort by "
                        // onClick={sortSection}
                    >
                        <IconButton  style={{
                            paddingTop:'37px', paddingLeft:"100px",
                        }} onClick={SortSection}>
                            <SortIcon />
                        </IconButton>
                        {reasons &&
                        reasons.map((reason) => (
                            <MenuItem key={reason} value={reason}>
                                <Checkbox checked={data.includes(reason)}/>
                                <ListItemText primary={reason}/>

                            </MenuItem>
                        ))}
                        <IconButton  style={{
                            paddingTop:'37px', paddingLeft:"100px",
                        }} onClick={SortSection}>
                            sort
                            <SortIcon />
                        </IconButton>
                    </Select>
                </FormControl>

            </div>
            <div>
                <h3>
                    Search Results
                </h3>
            {filterView === false ? (

                    <ResultCard searchResults={searchResults} handleFavouritesClick={addFavouriteMovie} />

            ) : (
             <SortSection/>
            )}

            </div>
            <div>
            <h3>
                Bookmarks
            </h3>
                <ResultCard searchResults={favourites} />
            </div>

      </>
    );
}



export default Search;
