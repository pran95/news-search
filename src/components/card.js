import React, {useEffect, useState} from 'react';
import TextField from '@material-ui/core/TextField';
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import axios from "axios";
import {FormControl, Input, InputLabel, MenuItem, Select} from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import ListItemText from "@material-ui/core/ListItemText";
import {FormControlLabel} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import SortIcon from '@material-ui/icons/Sort';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 545,
        backgroundColor: "lightslategray",

    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
        fontSize: '0.5em',
        width: 80,
        height: 80,
    },
    formControl: {
        // margin: theme.spacing(1),
        maxWidth: 200,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const ResultCard = ({ searchResults, handleFavouritesClick }) => {

    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);



    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
   ;



    return (
        <>

            { searchResults && searchResults.map( (story, index) => {
                return(
                    <div style={{display:"flex", justifyContent:"center", paddingBottom:'20px'}}>
                        <Card className={classes.root}>
                            <CardHeader
                                avatar={
                                    <Avatar aria-label="recipe" className={classes.avatar}>
                                        {story.sectionId}
                                    </Avatar>
                                }

                                title={story.title}
                                subheader={new Date(story.date).toDateString()}
                            />
                            <CardMedia
                                className={classes.media}
                                image={story.image}
                                title="Image"
                            />
                            <CardContent>
                                <Typography variant="body2" color="textSecondary" component="p">

                                    <a href={story.url}>{story.url}</a>
                                </Typography>
                            </CardContent>

                            <CardActions disableSpacing>
                                <IconButton style={{fontSize: "0.5em"}} aria-label="add to favorites"  onClick={() => handleFavouritesClick(story)}>
                                    <FavoriteIcon />  Add to Bookmarks
                                </IconButton>

                                <IconButton
                                    className={clsx(classes.expand, {
                                        [classes.expandOpen]: expanded,
                                    })}
                                    onClick={handleExpandClick}
                                    aria-expanded={expanded}
                                    aria-label="show more"
                                >
                                    <ExpandMoreIcon />
                                </IconButton>
                            </CardActions>
                            <Collapse in={expanded} timeout="auto" unmountOnExit>
                                <CardContent>
                                    <Typography variant="body2" color="textSecondary" component="p">
                                        {story.descp}
                                    </Typography>
                                </CardContent>
                            </Collapse>
                        </Card>
                    </div>


                )} )}
        </>
    );
};



export default ResultCard;
